'use strict';

var build_dir = 'assets/';

var gulp = require('gulp'),
    watch = require('gulp-watch'),
    prefixer = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    rigger = require('gulp-rigger'),
    cssmin = require('gulp-minify-css'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    rimraf = require('rimraf');

var folders = {
    src: 'src/',
    dst: ''
};

var path = {
    build: {
        html: folders.dst + build_dir,
        js: folders.dst + build_dir + 'js/',
        css: folders.dst + build_dir +'css/',
        img: folders.dst + build_dir +'images/',
        fonts: folders.dst + build_dir + 'fonts/'
    },
    src: {
        html: folders.src + '*.html',
        js: folders.src + 'js/app.js',
        style: folders.src + 'styles/app.scss',
        custom: folders.src + 'styles/custom.scss',
        img: [
            folders.src + 'images/**/*.*'
        ],
        fonts: folders.src + 'fonts/**/*.*',
        fontA: folders.src + 'vendor/font-awesome/fonts/*.*'
    },
    watch: {
        html: folders.src + '**/*.html',
        js: folders.src + 'js/**/*.js',
        style: folders.src + 'styles/**/*.scss',
        img: [
            folders.src + 'images/**/*.*'
        ],
        fonts: folders.src + 'fonts/**/*.*',
        fontA: folders.src + 'vendor/font-awesome/fonts/*.*'
    }
};
console.log(path.build.html);

gulp.task('html:build', function () {
    gulp.src(path.src.html)
        .pipe(rigger())
        .pipe(gulp.dest(path.build.html));
});

gulp.task('js:build', function () {
    gulp.src(path.src.js)
        .pipe(rigger())
        //.pipe(sourcemaps.init())
        //.pipe(uglify())
        //.pipe(sourcemaps.write())
        .pipe(gulp.dest(path.build.js));
});

gulp.task('styles:build', function () {
    gulp.src(path.src.style)
        .pipe(sourcemaps.init())
        .pipe(sass({
            includePaths: ['src/styles/'],
            outputStyle: 'compressed',
            sourceMap: true,
            errLogToConsole: true
        }))
        .pipe(prefixer())
        .pipe(cssmin())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(path.build.css));

    gulp.src(path.src.custom)
        .pipe(sourcemaps.init())
        .pipe(sass({
            includePaths: ['src/styles/'],
            outputStyle: 'compact',
            sourceMap: true,
            errLogToConsole: true
        }))
        .pipe(prefixer())
        .pipe(gulp.dest(path.build.css));
});

gulp.task('images:build', function () {
    gulp.src(path.src.img)
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()],
            interlaced: true
        }))
        .pipe(gulp.dest(path.build.img));
});

gulp.task('fonts:build', function () {
    gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts));

    gulp.src(path.src.fontA)
        .pipe(gulp.dest(path.build.fonts));
});

gulp.task('build', [
    'html:build',
    'js:build',
    'styles:build',
    'fonts:build',
    'images:build'
]);


gulp.task('watch', function () {
    gulp.watch([path.watch.html], function (event, cb) {
        gulp.start('html:build');
    });
    gulp.watch([path.watch.style], function (event, cb) {
        gulp.start('styles:build');
    });
    gulp.watch([path.watch.js], function (event, cb) {
        gulp.start('js:build');
    });
    gulp.watch([path.watch.img], function (event, cb) {
        gulp.start('images:build');
    });
    gulp.watch([path.watch.fonts], function (event, cb) {
        gulp.start('fonts:build');
    });
});


gulp.task('default', ['build', 'watch']);