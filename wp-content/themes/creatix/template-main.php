<?php
/**
 * All sections view page template file
 *
 * Template Name: Main Template
 */

get_header(); ?>


<?php
if( have_rows('content') ):

    while ( have_rows('content') ) : the_row();

        if( get_row_layout() == 'content_section' ):

        	$alignment = get_sub_field('alignment');
        	$type = get_sub_field('addidtion');

        	if($type == "None"):
				$title = get_sub_field('section_title');
				$content = get_sub_field('section_content');
        		switch ($alignment){
        			case "Left":
                        ?>
<section class="content simple">
<div class="container-fluid">
    <div class="row">
        <div class="col-md-13 col-md-offset-1">
            <h2 class="section-title text-left"><?php echo $title; ?></h2>
            <?php echo $content; ?>
        </div>
    </div>
</div>
</section>
<div class="clearfix"></div>
<?php
						break;
        			case "Center":
        				?>
<section class="content simple">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-9 col-md-offset-3 col-md-push-0">
                <h2 class="section-title text-center"><?php echo $title; ?></h2>
                <p class="text-center"><?php echo strip_tags($content, '<br>'); ?></p>
            </div>
        </div>
    </div>
</section>
<div class="clearfix"></div>
                        <?php
						break;
        		}


        	elseif ($type == "Image"):
				$title = get_sub_field('section_title');
				$content = get_sub_field('section_content');
				$image = get_sub_field('image');
        		switch ($alignment){
        			case "Left":
        				?>
<section class="content simple">
    <div class="container-fluid">
        <div class="row row-inline">
            <div class="col-md-6 col-md-offset-1 col-md-push-7 col-xs-15 col-xs-offset-0 text-center">
                <img class="img-responsive visible-xs-inline-block visible-sm-inline-block visible-md visible-lg" src="<?php echo $image; ?>" alt="content-image">
            </div>
            <div class="col-md-6 col-md-offset-1 col-md-pull-7">
                <h2 class="section-title text-left"><?php echo $title; ?></h2>
                <p class="text-left"><?php echo strip_tags($content, '<br>'); ?></p>
            </div>
        </div>
    </div>
</section>
<div class="clearfix"></div>
<?php
						break;

        			case "Right":
        				?>
<section class="content simple">
    <div class="container-fluid">
        <div class="row row-inline">
            <div class="col-md-6 col-md-offset-1 col-xs-15 col-xs-offset-0 text-center">
                <img class="img-responsive visible-xs-inline-block visible-sm-inline-block visible-md visible-lg" src="<?php echo $image; ?>" alt="content-image">
            </div>
            <div class="col-md-6 col-md-offset-1">
                <h2 class="section-title text-left"><?php echo $title; ?></h2>
                <p class="text-left"><?php echo strip_tags($content, '<br>'); ?></p>
            </div>
        </div>
    </div>
</section>
<div class="clearfix"></div>
<?php
						break;

        			case "Center":
        				?>
<section class="content simple">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-9 col-md-offset-3 col-md-push-0">
                <h2 class="section-title text-center"><?php echo $title; ?></h2>
                <p class="text-center margin-bottom-30"><?php echo strip_tags($content, '<br>'); ?></p>
            </div>
            <div class="col-md-15 col-md-offset-0 col-md-pull-0">
                <img class="img-responsive fullwidth" src="<?php echo $image; ?>" alt="content-image">
            </div>
        </div>
    </div>
</section>
<div class="clearfix"></div>
<?php
						break;
        		}


        	elseif ($type == "Gallery"):
				$title = get_sub_field('section_title');
				$content = get_sub_field('section_content');
				$images = get_sub_field('gallery');
        		switch ($alignment){
        			case "Left":
        				?>
<section class="content simple">
    <div class="container-fluid">
        <div class="row row-inline">
            <div class="col-sm-5 col-sm-offset-1">
                <h2 class="section-title text-left"><?php echo $title; ?></h2>
                <p class="text-left"><?php echo strip_tags($content, '<br>'); ?></p>
            </div>

            <div class="col-sm-7 col-sm-offset-1 col-xs-15">
                <div class="gallery-sm">
                    <div class="row">
                        <?php if( $images ): ?>
                        <?php foreach( $images as $image ): ?>
                        <div class="col-sm-7n5">
                            <div class="gallery-img" style="background-image: url(<?php echo $image['url']; ?>)"></div>
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/10x10.png" alt="pusher">
                        </div>
                        <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="clearfix"></div>
<?php
        				break;

        			case "Right":
        				?>
<section class="content simple">
    <div class="container-fluid">
        <div class="row row-inline">
            <div class="col-sm-7 col-sm-offset-1 col-xs-15">
                <div class="gallery-sm">
                    <div class="row">
                        <?php if( $images ): ?>
                        <?php foreach( $images as $image ): ?>
                        <div class="col-sm-7n5">
                            <div class="gallery-img" style="background-image: url(<?php echo $image['url']; ?>)"></div>
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/10x10.png" alt="pusher">
                        </div>
                        <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>

            <div class="col-sm-5 col-sm-offset-1">
                <h2 class="section-title text-left"><?php echo $title; ?></h2>
                <p class="text-left"><?php echo strip_tags($content, '<br>'); ?></p>
            </div>
        </div>
    </div>
</section>
<div class="clearfix"></div>
<?php
        				break;

        			case "Center":
        				?>
<section class="content simple">
    <div class="container-fluid">
        <div class="row row-inline">
            <div class="col-sm-13 col-sm-offset-1 col-xs-15">
                <h2 class="section-title text-left"><?php echo $title; ?></h2>
                <p class="text-left"><?php echo strip_tags($content, '<br>'); ?></p>
            </div>

            <div class="col-sm-13 col-sm-offset-1 col-xs-15">
                <div class="gallery-lg">
                    <div class="row">
                        <?php if( $images ): ?>
                        <?php foreach( $images as $image ): ?>
                        <div class="col-sm-7n5">
                            <div class="gallery-img" style="background-image: url(<?php echo $image['url']; ?>)"></div>
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/10x10.png" alt="pusher">
                        </div>
                        <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="clearfix"></div>
<?php
        				break;
        			}


        	elseif ($type == "Slider"):
				$title = get_sub_field('section_title');
				$content = get_sub_field('section_content');
				$images = get_sub_field('slider');
				switch ($alignment){
					case "Left":
						?>
<section class="content simple">
    <div class="container-fluid">
        <div class="row row-inline">
            <div class="col-sm-5 col-sm-offset-1 col-xs-15">
                <h2 class="section-title text-left"><?php echo $title; ?></h2>
                <p class="text-left"><?php echo strip_tags($content, '<br>'); ?></p>
            </div>
            <div class="col-sm-7 col-sm-offset-1 col-xs-15">
                <div class="slider content-slider owl-carousel">
                    <?php if( $images ): ?>
                    <?php foreach( $images as $image ): ?>
                    <div class="item text-center">
                        <img src="<?php echo $image['url']; ?>" alt="slider-img">
                    </div>
                    <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="clearfix"></div>
<?php
						break;

					case "Right":
						?>
<section class="content simple">
    <div class="container-fluid">
        <div class="row row-inline">
            <div class="col-sm-7 col-sm-offset-1 col-xs-15">
                <div class="slider content-slider owl-carousel">
                    <?php if( $images ): ?>
                    <?php foreach( $images as $image ): ?>
                    <div class="item text-center">
                        <img src="<?php echo $image['url']; ?>" alt="slider-img">
                    </div>
                    <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-sm-5 col-sm-offset-1 col-xs-15">
                <h2 class="section-title text-left"><?php echo $title; ?></h2>
                <p class="text-left"><?php echo strip_tags($content, '<br>'); ?></p>
            </div>
        </div>
    </div>
</section>
<div class="clearfix"></div>
<?php
						break;
					}


        	elseif ($type == "Slider (2rows)"):
				$title = get_sub_field('section_title');
				$content = get_sub_field('section_content');
				$items = get_sub_field('slider_2rows');
				?>
<section class="content simple">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-md-offset-3">
                <h2 class="section-title text-center"><?php echo $title; ?></h2>
                <div class="content-wrapper margin-bottom-65"><p class="text-center"><?php echo strip_tags($content, '<br>'); ?><p></div>
            </div>
        </div>
    </div>


    <?php if( have_rows('slider_2rows') ): ?>
    <?php $i=0; ?>
    <div class="slider slider-fluid right">
        <div class="scroll-wrapper">
            <div class="slider-wrapper">
                <?php while ( have_rows('slider_2rows') ) : the_row(); ?>
                <?php $i++; ?>
                <?php $work_post = get_sub_field('gallery_item'); ?>
                <?php $work_id = $work_post -> ID; ?>

                <?php if ($i % 2 == 0 && get_the_post_thumbnail_url($work_id)): ?>
                <div class="work-item item" style="background-image: url(<?php echo get_the_post_thumbnail_url($work_id); ?>);">
                    <a href="<?php echo get_permalink($id); ?>">
															<span class="slider-item-content">
																<span class="slider-item-title">
																	<?php echo get_the_title($work_id); ?>
																</span>
																<span class="slider-item-desc">
																	<?php echo get_the_excerpt($work_id); ?>
																</span>
															</span>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/10x10.png" alt="pusher">
                    </a>
                </div>
                <?php endif; ?>

                <?php endwhile; ?>
            </div>
        </div>
        <div class="nav slider-nav">
            <span class="prev"></span>
            <span class="next"></span>
        </div>
    </div>

    <div class="slider slider-fluid">
        <div class="scroll-wrapper">
            <div class="slider-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-14 col-sm-offset-1">

                            <?php while ( have_rows('slider_2rows') ) : the_row(); ?>
                            <?php $i++; ?>
                            <?php $work_post = get_sub_field('gallery_item'); ?>
                            <?php $work_id = $work_post -> ID; ?>

                            <?php if ($i% 2 != 0 && get_the_post_thumbnail_url($work_id)): ?>
                            <div class="work-item item" style="background-image: url(<?php echo get_the_post_thumbnail_url($work_id); ?>);">
                                <a href="<?php echo get_permalink($id); ?>">
																<span class="slider-item-content">
																	<span class="slider-item-title">
																		<?php echo get_the_title($work_id); ?>
																	</span>
																	<span class="slider-item-desc">
																		<?php echo get_the_excerpt($work_id); ?>
																	</span>
																</span>
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/10x10.png" alt="pusher">
                                </a>
                            </div>
                            <?php endif; ?>

                            <?php endwhile; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="nav slider-nav">
            <span class="prev"></span>
            <span class="next"></span>
        </div>
    </div>
    <?php else : ?>

    <?php endif; ?>
</section>
<div class="clearfix"></div>
<?php


        	elseif ($type == "Icons List"):
				$title = get_sub_field('section_title');
				$content = get_sub_field('section_content');
        		switch ($alignment){
					case "Left":
					?>
<section class="content three-col">
    <div class="col-md-13 col-md-offset-1">
        <h2 class="section-title text-left"><?php echo $title; ?></h2>
        <?php if( have_rows('icons_list') ): ?>
        <div class="content-container">
            <div class="content-row">
                <?php while ( have_rows('icons_list') ) : the_row(); ?>
                <div class="content-col text-center">
                    <img class="icon" src="<?php echo get_sub_field('icon'); ?>" alt="icon">
                    <h4 class="content-title "><?php echo get_sub_field('item_title'); ?></h4>
                    <p><?php echo strip_tags(get_sub_field('item_content'), '<br>'); ?></p>
                    <a href="<?php echo get_sub_field('item_button_url'); ?>" class="btn btn-primary btn-md"><?php echo get_sub_field('item_button'); ?></a>
                </div>
                <?php endwhile; ?>
            </div>
        </div>

        <?php else : ?>

        <?php endif; ?>
    </div>
</section>
<div class="clearfix"></div>
<?php
					break;

					case "Center"
					?>
<section class="content simple">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-9 col-md-offset-3 col-md-push-0">
                <h2 class="section-title text-center"><?php echo $title; ?></h2>
                <p class="text-center"><?php echo strip_tags($content, '<br>'); ?></p>
            </div>
        </div>
    </div>
    <div class="container">
        <?php if( have_rows('icons_list') ): ?>
        <div class="row text-center content-cols margin-top-130">
            <?php while ( have_rows('icons_list') ) : the_row(); ?>
            <div class="col-sm-5">
                <div class="icon-wrapper"><img class="icon" src="<?php echo get_sub_field('icon'); ?>"></div>
                <h4><?php echo get_sub_field('item_title'); ?></h4>
                <p><?php echo strip_tags(get_sub_field('item_content'), '<br>'); ?></p>
            </div>
            <?php endwhile; ?>
        </div>
        <?php else : ?>

        <?php endif; ?>
    </div>
</section>
<div class="clearfix"></div>
<?php
					break;
				}
		endif; //end of content


		elseif( get_row_layout() == 'blocks' ):
			$title = get_sub_field('blocks_section_title');
			$images = get_sub_field('block_list');
			?>
<section class="content blocks-content">
    <div class="col-md-12 col-md-offset-3">
        <h2 class="section-title text-left">What you can expect</h2>
    </div>

    <?php if( have_rows('blocks_list') ): ?>
    <div class="blocks-holder">
        <ul>
            <?php while ( have_rows('blocks_list') ) : the_row(); ?>
            <?php $block_title = get_sub_field('block_title'); ?>
            <?php $block_content = get_sub_field('block_content'); ?>
            <li>
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/10x10.png" alt="pusher">
                <h3 class="block-title"><?php echo $block_title; ?></h3>
                <div class="block-content">
                    <p><?php echo strip_tags($block_content, '<br>'); ?></p>
                </div>
            </li>
            <?php endwhile; ?>
        </ul>
    </div>
    <?php endif; ?>
</section>
<div class="clearfix"></div>
<?php


		elseif( get_row_layout() == 'slider_separated' ):
			$title = get_sub_field('slider_section_title');
			$images = get_sub_field('slider_content_separated');
			?>
<section class="slider slider-separated">
    <div class="container-fluid">
        <h2 class="section-title text-center"><?php echo $title; ?></h2>
        <?php if( $images ): ?>
        <div class="row">
            <div class="col-md-13 col-md-offset-1">
                <div class="owl-carousel">
                    <?php foreach( $images as $image ): ?>
                    <div class="item" style="background-image: url(<?php echo $image['url']; ?>);">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/10x10.png" alt="pusher">
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
        <?php endif; ?>
    </div>
</section>
<div class="clearfix"></div>
<?php


		elseif( get_row_layout() == 'testimonial' ):
			$title = get_sub_field('testimonial_section_title');
			$content = get_sub_field('testimonial_content');
			$author_name = get_sub_field('author');
			$author_position = get_sub_field('author_position');
			$image = get_sub_field('background_image');
			?>
<section class="content testimonial" style="background-image: url(<?php echo $image; ?>);">
    <div class="overlay"></div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-13 col-md-offset-1">
                <h2 class="section-title text-white text-center"><?php echo $title; ?></h2>
            </div>
            <div class="col-md-11 col-md-offset-2">
                <blockquote>
                    <p><?php echo strip_tags($content, '<br>'); ?></p>
                    <span class="author"><?php echo $author_name; ?></span>
                    <span class="position"><?php echo $author_position; ?></span>
                </blockquote>
            </div>
        </div>
    </div>
</section>
<div class="clearfix"></div>
<?php

		elseif( get_row_layout() == 'timeline' ):
        $title = get_sub_field('timeline_section_title');
			?>
<section class="timeline-content">
    <div class="col-md-13 col-md-offset-1">
        <h2 class="section-title text-left"><?php echo $title; ?></h2>
    </div>

    <?php if( have_rows('timeline_section') ): ?>
    <div class="col-md-13 col-md-offset-1">
        <ul class="timeline">
            <?php while ( have_rows('timeline_section') ) : the_row(); ?>
            <?php $title = get_sub_field('timeline_title'); ?>
            <?php $content = get_sub_field('timeline_content'); ?>
            <?php $icon = get_sub_field('block_icon'); ?>
            <li>
                <div class="stage">
                    <h4><img src="<?php echo $icon; ?>" alt="icon"><?php echo $title; ?></h4>
                    <p><?php echo strip_tags($content, '<br>'); ?></p>
                </div>
            </li>
            <?php endwhile; ?>
        </ul>
    </div>
    <?php else : ?>
    <?php endif; ?>
</section>
<div class="clearfix"></div>
<?php

		elseif( get_row_layout() == 'divider' ):
			?>
<section class="section-divider">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-13 col-sm-offset-1 col-xs-15 col-xs-offset-0">
                <div class="divider"></div>
            </div>
        </div>
    </div>
</section>
<div class="clearfix"></div>
<?php

		elseif( get_row_layout() == 'references' ):
		    $title = get_sub_field('reference_section_title');
			?>
<section class="works-grid">

    <div class="col-md-13 col-md-offset-1">
        <h2 class="section-title text-center"><?php echo $title; ?></h2>
    </div>
    <?php if( have_rows('project_reference', 'options') ): ?>
    <div class="grid-wrapper">
    <?php while ( have_rows('project_reference', 'options') ) : the_row(); ?>
    <?php $i++; ?>
    <?php $work_post = get_sub_field('project'); ?>
    <?php $work_id = $work_post -> ID; ?>
            <div class="col-sm-5">
                <div class="row">
                    <div class="work-item item" style="background-image: url(<?php echo get_the_post_thumbnail_url($work_id); ?>)">
                        <div class="post-title-hidden hidden">
                            <?php echo getContent($work_id)[0]; ?>
                        </div>
                        <div class="post-content-hidden hidden">
                            <?php echo getContent($work_id)[1]; ?>
                        </div>
                        <a href="<?php echo get_permalink($id); ?>">
                            <span class="slider-item-content">
                                <span class="slider-item-title">
                                    <?php echo get_the_title($work_id); ?>
                                </span>
                                <span class="slider-item-desc">
                                    <?php echo get_the_excerpt($work_id); ?>
                                </span>
                            </span>
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/10x10.png" alt="pusher">
                        </a>
                    </div>
                </div>
            </div>
    <?php endwhile; ?>
    </div>
    <?php else : ?>

    <?php endif; ?>
</section>
<?php
		endif;

    endwhile;

else :

endif;
?>

<?php if( is_page( 'contact-us' ) ) : ?>

<div class="clearfix"></div>
<?php if( have_rows('locations', options) ): ?>
    <?php $locations = "['"; ?>
    <?php while ( have_rows('locations', options) ) : the_row(); ?>
        <?php $locations .= get_sub_field('location_address', options) . "', '" ; ?>
    <?php endwhile; ?>
    <?php $locations .= "']"; ?>
<?php else : ?>
    <?php $locations = "New York"; ?>
<?php endif; ?>
<div 	class="map"
        data-address="<?php echo $locations; ?>"
        data-marker="<?php echo get_template_directory_uri(); ?>/assets/images/icons/marker.png"
        data-zoom="16">
</div>

<section class="contacts">
    <div class="col-md-6 col-md-offset-1">
        <h2 class="section-title text-left">Our Locations</h2>
        <div class="locations">
            <?php if( have_rows('locations', options) ): ?>
                <?php while ( have_rows('locations', options) ) : the_row(); ?>
                    <?php $location_title = get_sub_field('location_title', options); ; ?>
                    <?php $location_address = get_sub_field('location_address', options) ; ?>
                    <?php $location_email = get_sub_field('location_email', options) ; ?>
                    <h4><?php echo $title; ?></h4>
                    <p>
                        <?php echo strip_tags($location_address, '<br>'); ?><br>
                        Email: <a href="mailto:<?php echo $location_email; ?>"><?php echo $location_email; ?></a>
                    </p>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </div>
    <div class="col-md-7">
        <?php echo do_shortcode('[contact-form-7 id="114" title="Contact"]'); ?>
    </div>
</section>
    <div class="clearfix"></div>


<?php endif; ?>

<?php get_footer();

