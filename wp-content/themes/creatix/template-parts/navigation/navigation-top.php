<?php
/**
 * Displays top navigation
 *
 * @package WordPress
 * @subpackage Creatix
 * @since 1.0
 * @version 1.2
 */

?>
<nav id="site-navigation" class="main-navigation navbar navbar-default" role="navigation" aria-label="<?php esc_attr_e( 'Top Menu', 'creatix' ); ?>">

	<!-- Brand and toggle get grouped for better mobile display -->
	<div class="navbar-header">
		<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar top-bar"></span>
			<span class="icon-bar middle-bar"></span>
			<span class="icon-bar bottom-bar"></span>
		</button>
		<a class="navbar-brand" href="<?php echo get_home_url();?>">CREATIX</a>
	</div>

	<!-- Collect the nav links, forms, and other content for toggling -->
	<?php
		wp_nav_menu( array(
			'menu'              => 'Header Menu',
			'theme_location'    => 'creatix',
			'depth'             => 2,
			'container'         => 'div',
			'container_class'   => 'collapse navbar-collapse',
			'container_id'      => 'bs-example-navbar-collapse-1',
			'menu_class'        => 'nav navbar-nav navbar-right',
			'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
			'walker'            => new WP_Bootstrap_Navwalker())
		);
	?>
	<!-- /.navbar-collapse -->
</nav><!-- #site-navigation -->
