<?php
/**
 * Displays footer site info
 *
 * @package WordPress
 * @subpackage Creatix
 * @since 1.0
 * @version 1.0
 */

?>
<div class="site-info">
	<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'creatix' ) ); ?>"><?php printf( __( 'Proudly powered by %s', 'creatix' ), 'WordPress' ); ?></a>
</div><!-- .site-info -->
