<?php
/**
 * Template part for displaying audio posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Creatix
 * @since 1.0
 * @version 1.2
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-13 col-sm-offset-1">
				<h2><?php _e(get_the_title()); ?></h2>
				<a href="<?php echo get_day_link( get_the_date(Y), get_the_date(m), get_the_date(d) ); ?>" class="post-date"><?php echo get_the_date("j M Y"); ?></a>
				<div class="post-thumbnail" style="background-image: url(<?php _e(get_the_post_thumbnail_url($post->ID,'full')); ?>);"></div>
				<?php the_content(); ?>
			</div>
		</div>
	</div>
</article><!-- #post-## -->
