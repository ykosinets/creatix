<!--place-->

    <div class="tagline">
    <?php
        if( have_rows('locations', options) ):
        $i = 0;

        // loop through the rows of data
        while ( have_rows('locations', options) ) : the_row();

        $i++;

        // take first element
        if ($i == 1){
            // display a sub field value
                $address = strip_tags( get_sub_field('location_address', 'options'));
            echo '<ul>
                <li>Digital Agency</li>
                <li><a class="agency-link" href="' . get_home_url() . '/contact-us#map" data-location="' . $address . '">Hong Kong</a></li>
            </ul>';
            //echo get_sub_field('location_address');
        }

        endwhile;

        else :

        // no rows found

        endif;
    ?>

    <!--subpages-->
    <ul>
    <?php
        wp_list_pages(
            array(
                'title_li'    => '',
                'child_of'    => 24
            )
        );
    ?>
    </ul>
    </div>
