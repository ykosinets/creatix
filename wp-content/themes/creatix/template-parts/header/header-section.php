<div class="header-section" <?php if ( $img ) { echo 'style=" background-image: url(' . $img . ');"'; } ?> >
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-13 col-sm-offset-1">
                <h1 class="page-title text-red">
                    <?php if( is_single() || is_home() || is_archive() ) : ?>
                        Blog
                    <?php else : ?>
                        <?php if ( get_field('page_title') ) : ?>
                            <?php echo get_field('page_title'); ?>
                        <?php else : ?>
                            <?php echo get_the_title(); ?>
                        <?php endif; ?>
                    <?php endif; ?>
                </h1>
                <?php require_once('header-tagline.php'); ?>
                    <?php $btn_link = get_field('header_button_link'); ?>
                    <?php $btn_title = get_field('header_button_title'); ?>
                <?php if ( is_front_page() ) : ?>
                    <?php echo '<a href="' . get_home_url() . '/contuct-us" class="btn btn-default btn-lg">' . $btn_title . '</a>'; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>