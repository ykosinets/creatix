<?php
/**
 * All sections view page template file
 *
 * Template Name: Blog
 */

get_header(); ?>

home page

	<section class="section-divider">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-13 col-sm-offset-1 col-xs-15 col-xs-offset-0">
					<div class="divider"></div>
				</div>
			</div>
		</div>
	</section>
<div class="clearfix"></div>

	<section class="content three-col">
		<div class="col-md-13 col-md-offset-1">
			<h2 class="section-title text-left">What we do</h2>
			<div class="content-container">
				<div class="content-row">
					<div class="content-col text-center">
						<img class="icon" src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/icon-note.png" alt="icon">
						<h4 class="content-title ">Web Design & Development</h4>
						<p>Web Design lcome to Creatix, we are a Hong Kong based Web Design and Development studio. We are a
							team of designer and developer passtionate to deliver result for our client. We truly believe
							every business should have a stunning website and we are here to deliver it</p>
						<a class="btn btn-primary btn-md">Web Design</a>
					</div>
					<div class="content-col text-center">
						<img class="icon" src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/icon-basket.png" alt="icon">
						<h4 class="content-title">E-Commerce Website</h4>
						<p>E-Commerce lcome to Creatix, we are a Hong Kong based Web Design and Development studio. We are a
							team of designer and developer passtionate to deliver result for our client. We truly believe
							every business should have a stunning website and we are here to deliver it</p>
						<a class="btn btn-primary btn-md">E-Commerce</a>
					</div>
					<div class="content-col text-center">
						<img class="icon" src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/icon-lamp.png" alt="icon">
						<h4 class="content-title">Graphic & Print</h4>
						<p>Graphic Web Design lcome to Creatix, we are a Hong Kong based Web Design and Development studio.
							We are a team of designer and developer passtionate to deliver result for our client. We truly
							believe every business should have a stunning website and we are here to deliver it</p>
						<a class="btn btn-primary btn-md">Graphic & Print</a>
					</div>
				</div>
			</div>
		</div>
	</section>
<div class="clearfix"></div>

	<section class="section-divider">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-13 col-sm-offset-1 col-xs-15 col-xs-offset-0">
					<div class="divider"></div>
				</div>
			</div>
		</div>
	</section>
<div class="clearfix"></div>

	<section class="content blocks-content">
		<div class="col-md-12 col-md-offset-3">
			<h2 class="section-title text-left">What you can expect</h2>
		</div>


		<div class="blocks-holder">
			<ul>
				<li><h3 class="block-title">Non-Technical</h3>
					<div class="block-content">
						<p>E-Commerce lcome to Creatix, we are a Hong Kong based Web Design and Development studio. We
							are a team of designer and developer passtionate to deliver result for our client. We truly
							believe every business should have a stunning website and we are here to deliver it
							E-Commerce lcome to Creatix, we are a Hong Kong based Web Design and Development studio. We
							are a team of designer and developer passtionate to deliver result for our client. We truly
							believe every business should have a stunning website and we are here to deliver
							itE-Commerce lcome to Creatix, we are a Hong Kong based Web Design and Development studio.
							We are a team of designer and developer passtionate to deliver result for our client. We
							truly believe every business should have a stunning website and we are here to deliver it
							E-Commerce lcome to Creatix, we are a Hong Kong based Web Design and \</p>
					</div>
				</li>
				<li><h3 class="block-title">Flexible, Accomodating</h3>
					<div class="block-content">
						<p>E-Commerce lcome to Creatix, we are a Hong Kong based Web Design and Development studio. We
							are a team of designer and developer passtionate to deliver result for our client. We truly
							believe every business should have a stunning website and we are here to deliver it
							E-Commerce lcome to Creatix, we are a Hong Kong based Web Design and Development studio. We
							are a team of designer and developer passtionate to deliver result for our client. We truly
							believe every business should have a stunning website and we are here to deliver
							itE-Commerce lcome to Creatix, we are a Hong Kong based Web Design and Development studio.
							We are a team of designer and developer passtionate to deliver result for our client. We
							truly believe every business should have a stunning website and we are here to deliver it
							E-Commerce lcome to Creatix, we are a Hong Kong based Web Design and \</p>
					</div>
				</li>
				<li><h3 class="block-title">Passionately Creative</h3>
					<div class="block-content">
						<p>E-Commerce lcome to Creatix, we are a Hong Kong based Web Design and
							Development studio. We are a team of designer and developer passtionate to deliver result for
							our client. We truly believe every business should have a stunning website and we are here to
							deliver it E-Commerce lcome to Creatix, we are a Hong Kong based Web Design and Development
							studio. We are a team of designer and developer passtionate to deliver result for our client. We
							truly believe every business should have a stunning website and we are here to deliver
							itE-Commerce lcome to Creatix, we are a Hong Kong based Web Design and Development studio. We
							are a team of designer and developer passtionate to deliver result for our client. We truly
							believe every business should have a stunning website and we are here to deliver it E-Commerce
							lcome to Creatix, we are a Hong Kong based Web Design and \</p>
					</div>
				</li>
			</ul>
		</div>
	</section>
<div class="clearfix"></div>

	<section class="content simple">
		<div class="container-fluid">
			<div class="row row-inline">
				<div class="col-md-6 col-md-offset-1 col-md-push-8">
					<img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/images/content-img-1.jpg" alt="content-image">
				</div>
				<div class="col-md-7 col-md-offset-1 col-md-pull-7">
					<h2 class="section-title text-left">Hello!</h2>
					<p class="text-left">come to Creatix, we are a Hong Kong based Web Design and Development studio. We are
						a team of designer and developer passtionate to deliver result for our client. We truly believe
						every business should have a stunning website and we are here to deliver it lcome to Creatix, we are
						a Hong Kong based Web Design and Development studio. We are a team of designer and developer
						passtionate to deliver result for our client. We truly believe every business should have a stunning
						website and we are here to deliver it lcome to Creatix, we are a Hong Kong based Web Design and
						Development studio. We are a team of designer and developer passtionate to deliver result for our
						client. We truly believe every business should have a stunning website and we are here to deliver it
					</p>
				</div>
			</div>
		</div>
	</section>
<div class="clearfix"></div>

	<section class="content testimonial" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/images/bg-testimonial.jpg);">
		<div class="overlay"></div>
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-13 col-md-offset-1">
					<h2 class="section-title text-white text-center">What they say</h2>
				</div>
				<div class="col-md-11 col-md-offset-2">
					<blockquote>
						<p>
							“ for our client. We truly believe every business should have a stunning website and we are here
							to deliver itlcome to Creatix, we are a Hong Kong based Web Design and Development studio. We
							are a team of designer and developer passtionate to deliver result for our client. We truly
							believe every business should have a stunning website and we are here to deliver it ”
						</p>
						<span class="author">Peter Chan</span>
						<span class="position">Head of Marketing, Company Name</span>
					</blockquote>
				</div>
			</div>
		</div>
	</section>
<div class="clearfix"></div>

	<section class="slider slider-separated">
	<div class="container-fluid">
		<h2 class="section-title text-center">Join Their Success</h2>
		<div class="row">
			<div class="col-md-13 col-md-offset-1">
				<div class="owl-carousel">
					<div class="item" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/images/slide-1.jpg);">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/10x10.png" alt="pusher">
					</div>
					<div class="item" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/images/slide-1.jpg);">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/10x10.png" alt="pusher">
					</div>
					<div class="item" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/images/slide-1.jpg);">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/10x10.png" alt="pusher">
					</div>
					<div class="item" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/images/slide-1.jpg);">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/10x10.png" alt="pusher">
					</div>
					<div class="item" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/images/slide-1.jpg);">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/10x10.png" alt="pusher">
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<div class="clearfix"></div>

what we do page (design)

	<section class="content simple">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-9 col-md-offset-3 col-md-push-0 margin-top-200">
					<h2 class="section-title text-center">Web Design Hong Kong</h2>
					<p class="text-center margin-bottom-30">come to Creatix, we are a Hong Kong based Web Design and Development studio. We are
						a team of designer and developer passtionate to deliver result for our client. We truly believe
						every business should have a stunning website and we are here to deliver it lcome to Creatix, we are
						a Hong Kong based Web Design and Development studio. We are a team of designer and developer
						passtionate to deliver result for our client. We truly believe every business should have a stunning
						website and we are here to deliver it lcome to Creatix, we are a Hong Kong based Web Design and
						Development studio. We are a team of designer and developer passtionate to deliver result for our
						client. We truly believe every business should have a stunning website and we are here to deliver it
					</p>
				</div>
				<div class="col-md-13 col-md-offset-1 col-md-pull-0">
					<img class="img-responsive fullwidth" src="<?php echo get_template_directory_uri(); ?>/assets/images/content-img-3.jpg" alt="content-image">
				</div>
			</div>
		</div>
	</section>
<div class="clearfix"></div>

	<section class="timeline-content">
		<div class="col-md-13 col-md-offset-1">
			<h2 class="section-title text-left">Our Process</h2>
		</div>

		<ul class="timeline">
			<li>
				<div class="stage">
					<h4>Project Briefing & Planning</h4>
					<p>E-Commerce lcome to Creatix, we are a Hong Kong based Web Design and Development studio. We are a
						team of designer and developer passtionate to deliver result for our client. We truly believe every
						business should have a stunning website and we are here to deliver it E-Commerce lcome to Creatix,
						we are a Hong Kong based Web Design and Development studio. We are a team of designer and developer
						passtionate to deliver result for our client. We truly believe every business should have a stunning
						website and we are here to </p>
				</div>
			</li>
			<li>
				<div class="stage">
					<h4>Mockup</h4>
					<p>E-Commerce lcome to Creatix, we are a Hong Kong based Web Design and Development studio. We are a
						team of designer and developer passtionate to deliver result for our client. We truly believe every
						business should have a stunning website and we are here to deliver it E-Commerce lcome to Creatix,
						we are a Hong Kong based Web Design and Development studio. We are a team of designer and developer
						passtionate to deliver result for our client. We truly believe every business should have a stunning
						website and we are here to </p>
				</div>
			</li>
			<li>
				<div class="stage">
					<h4>Project Briefing & Planning</h4>
					<p>E-Commerce lcome to Creatix, we are a Hong Kong based Web Design and Development studio. We are a
						team of designer and developer passtionate to deliver result for our client. We truly believe every
						business should have a stunning website and we are here to deliver it E-Commerce lcome to Creatix,
						we are a Hong Kong based Web Design and Development studio. We are a team of designer and developer
						passtionate to deliver result for our client. We truly believe every business should have a stunning
						website and we are here to </p>
				</div>
			</li>
			<li>
				<div class="stage">
					<h4>Mockup</h4>
					<p>E-Commerce lcome to Creatix, we are a Hong Kong based Web Design and Development studio. We are a
						team of designer and developer passtionate to deliver result for our client. We truly believe every
						business should have a stunning website and we are here to deliver it E-Commerce lcome to Creatix,
						we are a Hong Kong based Web Design and Development studio. We are a team of designer and developer
						passtionate to deliver result for our client. We truly believe every business should have a stunning
						website and we are here to </p>
				</div>
			</li>
		</ul>
	</section>
<div class="clearfix"></div>

	<section class="content simple margin-bottom-100">
		<div class="container">
			<div class="row">
				<div class="col-md-7 col-md-offset-4 col-sm-9 col-sm-offset-3">
					<h2 class="section-title text-center">Best Practice</h2>
					<p class="text-center">E-Commerce lcome to Creatix, we are a Hong Kong based Web Design and
						Development studio. We are a team of designer and developer passtionate to deliver result for
						our client. We truly believe every business should have a stunning website and we are here to
						deliver it E-Commerce lcome to Creatix, we are a Hong Kong based Web Design and Development
						studio. We are a team of designer and developer passtionate to deliver result for our client. We
						truly believe every business should have a stunning website and we are here to
					</p>
				</div>
			</div>
			<div class="row text-center content-cols margin-top-130">
				<div class="col-sm-5">
					<img class="icon icon-heart" src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/heart@2x.png">
					<h4>Visually Stunning</h4>
					<p>
						E-Commerce lcome to Creatix, we are a Hong Kong based Web Design and Development studio. We are
						a team of designer and developer passtionate to deliver result for our client.
					</p>
				</div>
				<div class="col-sm-5">
					<img class="icon icon-cmd" src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/cmd@2x.png">
					<h4>Visually Stunning</h4>
					<p>
						E-Commerce lcome to Creatix, we are a Hong Kong based Web Design and Development studio. We are
						a team of designer and developer passtionate to deliver result for our client.
					</p>
				</div>
				<div class="col-sm-5">
					<img class="icon icon-baloon" src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/baloon@2x.png">
					<h4>Visually Stunning</h4>
					<p>
						E-Commerce lcome to Creatix, we are a Hong Kong based Web Design and Development studio. We are
						a team of designer and developer passtionate to deliver result for our client.
					</p>
				</div>
				<div class="col-sm-5">
					<img class="icon icon-speedometer" src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/speedometer@2x.png">
					<h4>Visually Stunning</h4>
					<p>
						E-Commerce lcome to Creatix, we are a Hong Kong based Web Design and Development studio. We are
						a team of designer and developer passtionate to deliver result for our client.
					</p>
				</div>
				<div class="col-sm-5">
					<img class="icon icon-check" src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/check@2x.png">
					<h4>Visually Stunning</h4>
					<p>
						E-Commerce lcome to Creatix, we are a Hong Kong based Web Design and Development studio. We are
						a team of designer and developer passtionate to deliver result for our client.
					</p>
				</div>
				<div class="col-sm-5">
					<img class="icon icon-tablet" src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/tablet@2x.png">
					<h4>Visually Stunning</h4>
					<p>
						E-Commerce lcome to Creatix, we are a Hong Kong based Web Design and Development studio. We are
						a team of designer and developer passtionate to deliver result for our client.
					</p>
				</div>
			</div>
		</div>
	</section>
<div class="clearfix"></div>

	<section class="content simple">
		<div class="container-fluid">
			<div class="row row-inline">
				<div class="col-sm-6 col-sm-offset-1 col-sm-push-8">
					<img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/images/content-img-2.jpg" alt="content-image">
				</div>
				<div class="col-sm-7 col-sm-offset-1 col-sm-pull-7">
					<h2 class="section-title text-left">Peace of mind</h2>
					<p class="text-left">E-Commerce lcome to Creatix, we are a Hong Kong based Web Design and
						Development studio. We are a team of designer and developer passtionate to deliver result for
						our client. We truly believe every business should have a stunning website and we are here to
						deliver it E-Commerce lcome to Creatix, we are a Hong Kong based Web Design and Development
						studio. We are a team of designer and developer passtionate to deliver result for our client. We
						truly believe every business should have a stunning website and we are here to deliver
						itE-Commerce lcome to Creatix, we are a Hong Kong based Web Design and Development studio. We
						are a team of designer and developer passtionate to deliver result for our client. We truly
						believe every business should have a stunning website and we are here to deliver it E-Commerce
						lcome to Creatix, we are a Hong Kong based Web Design and \
					</p>
				</div>
			</div>
		</div>
		</section>
<div class="clearfix"></div>

	<section class="works-grid">

		<div class="col-md-13 col-md-offset-1">
			<h2 class="section-title text-center">Project Reference</h2>
		</div>

		<div class="grid-wrapper">
			<div class="col-sm-5">
				<div class="row">
					<div class="work-item item" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/images/bg-testimonial.jpg)">
						<a href="#">
					<span class="slider-item-content">
						<span class="slider-item-title">
							Logo Design
						</span>
						<span class="slider-item-desc">
							Client Name
						</span>
					</span>
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/10x10.png" alt="pusher">
						</a>
					</div>
				</div>
			</div>


			<div class="col-sm-5">
				<div class="row">
					<div class="work-item item" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/images/bg-testimonial.jpg)">
						<a href="#">
					<span class="slider-item-content">
						<span class="slider-item-title">
							Logo Design
						</span>
						<span class="slider-item-desc">
							Client Name
						</span>
					</span>
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/10x10.png" alt="pusher">
						</a>
					</div>
				</div>
			</div>


			<div class="col-sm-5">
				<div class="row">
					<div class="work-item item" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/images/bg-testimonial.jpg)">
						<a href="#">
					<span class="slider-item-content">
						<span class="slider-item-title">
							Logo Design
						</span>
						<span class="slider-item-desc">
							Client Name
						</span>
					</span>
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/10x10.png" alt="pusher">
						</a>
					</div>
				</div>
			</div>

			<div class="col-sm-5">
				<div class="row">
					<div class="work-item item" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/images/bg-testimonial.jpg)">
						<a href="#">
					<span class="slider-item-content">
						<span class="slider-item-title">
							Logo Design
						</span>
						<span class="slider-item-desc">
							Client Name
						</span>
					</span>
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/10x10.png" alt="pusher">
						</a>
					</div>
				</div>
			</div>


			<div class="col-sm-5">
				<div class="row">
					<div class="work-item item" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/images/bg-testimonial.jpg)">
						<a href="#">
					<span class="slider-item-content">
						<span class="slider-item-title">
							Logo Design
						</span>
						<span class="slider-item-desc">
							Client Name
						</span>
					</span>
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/10x10.png" alt="pusher">
						</a>
					</div>
				</div>
			</div>


			<div class="col-sm-5">
				<div class="row">
					<div class="work-item item" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/images/bg-testimonial.jpg)">
						<a href="#">
					<span class="slider-item-content">
						<span class="slider-item-title">
							Logo Design
						</span>
						<span class="slider-item-desc">
							Client Name
						</span>
					</span>
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/10x10.png" alt="pusher">
						</a>
					</div>
				</div>
			</div>

		</div>
	</section>
<div class="clearfix"></div>

what we do page (graphic)

	<section class="content simple">
		<div class="container-fluid">
			<div class="row row-inline">
				<div class="col-sm-7">
					<div class="row text-center">
						<img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/images/content-img-2.jpg" alt="content-image">
					</div>
				</div>
				<div class="col-sm-7">
					<h2 class="section-title text-left">Graphic Design Hong Kong</h2>
					<p class="text-left">come to Creatix, we are a Hong Kong based Web Design and Development studio. We are a
						team of designer and developer passtionate to deliver result for our client. We truly believe every
						business should have a stunning website and we are here to deliver it lcome to Creatix, we are a Hong
						Kong based Web Design and Development studio. We are a team of designer and developer passtionate to
						deliver result for our client. We truly believe every business should have a stunning website and we are
						here to deliver it lcome to Creatix, we are a Hong Kong based Web Design and Development studio. We are
						a team of designer and developer passtionate to deliver result for our client. We truly believe every
						business should have a stunning website and we are here to deliver it
					</p>
				</div>
			</div>
		</div>
	</section>
<div class="clearfix"></div>

	<section class="content simple">
		<div class="container-fluid">
			<div class="row row-inline">
				<div class="col-sm-7 col-sm-offset-1 col-xs-15">
					<div class="gallery-sm">
						<div class="row">
							<div class="col-sm-7n5">
								<div class="gallery-img" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/images/image-placeholder-red-10x10.png)"></div>
								<img src="<?php echo get_template_directory_uri(); ?>/assets/images/10x10.png" alt="pusher">
							</div>
							<div class="col-sm-7n5">
								<div class="gallery-img" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/images/image-placeholder-red-10x10.png)"></div>
								<img src="<?php echo get_template_directory_uri(); ?>/assets/images/10x10.png" alt="pusher">
							</div>
							<div class="col-sm-7n5">
								<div class="gallery-img" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/images/image-placeholder-red-10x10.png)"></div>
								<img src="<?php echo get_template_directory_uri(); ?>/assets/images/10x10.png" alt="pusher">
							</div>
							<div class="col-sm-7n5">
								<div class="gallery-img" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/images/image-placeholder-red-10x10.png)"></div>
								<img src="<?php echo get_template_directory_uri(); ?>/assets/images/10x10.png" alt="pusher">
							</div>
						</div>
					</div>
				</div>

				<div class="col-sm-5 col-sm-offset-1">
					<h2 class="section-title text-left">Logo Design</h2>
					<p class="text-left">come to Creatix, we are a Hong Kong based Web Design and Development studio. We are a
						team of designer and developer passtionate to deliver result for our client. We truly believe every
						business should have a stunning website and we are here to deliver it lcome to Creatix, we are a Hong
						Kong based Web Design and Development studio. We are a team of designer and developer passtionate to
						deliver result for our client. We truly believe every business should have a stunning website and we are
						here to deliver it lcome to Creatix, we are a Hong Kong based Web Design and Development studio. We are
						a team of designer and developer passtionate to deliver result for our client. We truly believe every
						business should have a stunning website and we are here to deliver it
					</p>
				</div>
			</div>
		</div>
	</section>
<div class="clearfix"></div>

	<section class="content simple">
		<div class="container-fluid">
			<div class="row row-inline">
				<div class="col-xs-13  col-sm-offset-1">
					<h2 class="section-title text-center">Logo Design</h2>
					<p class="text-center">come to Creatix, we are a Hong Kong based Web Design and Development studio. We are a
						team of designer and developer passtionate to deliver result for our client. We truly believe every
						business should have a stunning website and we are here to deliver it lcome to Creatix, we are a Hong
						Kong based Web Design and Development studio. We are a team of designer and developer passtionate to
						deliver result for our client. We truly believe every business should have a stunning website and we are
						here to deliver it lcome to Creatix, we are a Hong Kong based Web Design and Development studio. We are
						a team of designer and developer passtionate to deliver result for our client. We truly believe every
						business should have a stunning website and we are here to deliver it
					</p>
				</div>

				<div class="col-xs-13  col-sm-offset-1">
					<div class="gallery-lg">
						<div class="row">
							<div class="col-sm-7n5">
								<div class="gallery-img" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/images/image-placeholder-red-10x10.png)"></div>
								<img src="<?php echo get_template_directory_uri(); ?>/assets/images/10x10.png" alt="pusher">
							</div>
							<div class="col-sm-7n5">
								<div class="gallery-img" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/images/image-placeholder-red-10x10.png)"></div>
								<img src="<?php echo get_template_directory_uri(); ?>/assets/images/10x10.png" alt="pusher">
							</div>
							<div class="col-sm-7n5">
								<div class="gallery-img" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/images/image-placeholder-red-10x10.png)"></div>
								<img src="<?php echo get_template_directory_uri(); ?>/assets/images/10x10.png" alt="pusher">
							</div>
							<div class="col-sm-7n5">
								<div class="gallery-img" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/images/image-placeholder-red-10x10.png)"></div>
								<img src="<?php echo get_template_directory_uri(); ?>/assets/images/10x10.png" alt="pusher">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
<div class="clearfix"></div>

	<section class="content simple">
		<div class="container-fluid">
			<div class="row row-inline">
				<div class="col-sm-7 col-sm-offset-1 col-xs-15">
					<div class="slider content-slider owl-carousel">
						<div class="item text-center">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/image-placeholder-red-10x10.png" alt="slider-img">
						</div>
						<div class="item text-center">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/image-placeholder-red-10x10.png" alt="slider-img">
						</div>
						<div class="item text-center">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/image-placeholder-red-10x10.png" alt="slider-img">
						</div>
					</div>
				</div>
				<div class="col-sm-5 col-sm-offset-1 col-xs-15">
					<h2 class="section-title text-left">Logo Design</h2>
					<p class="text-left">come to Creatix, we are a Hong Kong based Web Design and Development studio. We are a
						team of designer and developer passtionate to deliver result for our client. We truly believe every
						business should have a stunning website and we are here to deliver it lcome to Creatix, we are a Hong
						Kong based Web Design and Development studio. We are a team of designer and developer passtionate to
						deliver result for our client. We truly believe every business should have a stunning website and we are
						here to deliver it lcome to Creatix, we are a Hong Kong based Web Design and Development studio. We are
						a team of designer and developer passtionate to deliver result for our client. We truly believe every
						business should have a stunning website and we are here to deliver it
					</p>
				</div>
			</div>
		</div>
	</section>
<div class="clearfix"></div>

	<section class="content simple">
		<div class="container-fluid">
			<div class="row row-inline">
				<div class="col-sm-5 col-sm-offset-1 col-xs-15">
					<h2 class="section-title text-left">Logo Design</h2>
					<p class="text-left">come to Creatix, we are a Hong Kong based Web Design and Development studio. We are a
						team of designer and developer passtionate to deliver result for our client. We truly believe every
						business should have a stunning website and we are here to deliver it lcome to Creatix, we are a Hong
						Kong based Web Design and Development studio. We are a team of designer and developer passtionate to
						deliver result for our client. We truly believe every business should have a stunning website and we are
						here to deliver it lcome to Creatix, we are a Hong Kong based Web Design and Development studio. We are
						a team of designer and developer passtionate to deliver result for our client. We truly believe every
						business should have a stunning website and we are here to deliver it
					</p>
				</div>
				<div class="col-sm-7 col-sm-offset-1 col-xs-15">
					<div class="slider content-slider owl-carousel">
						<div class="item text-center">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/image-placeholder-red-10x10.png" alt="slider-img">
						</div>
						<div class="item text-center">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/image-placeholder-red-10x10.png" alt="slider-img">
						</div>
						<div class="item text-center">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/image-placeholder-red-10x10.png" alt="slider-img">
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
<div class="clearfix"></div>

	<section class="content simple">
		<div class="container-fluid">
			<div class="row row-inline">
				<div class="col-md-7 col-md-push-7">
					<h2 class="section-title text-left">Logo Design</h2>
					<p class="text-left">come to Creatix, we are a Hong Kong based Web Design and Development studio. We are a
						team of designer and developer passtionate to deliver result for our client. We truly believe every
						business should have a stunning website and we are here to deliver it lcome to Creatix, we are a Hong
						Kong based Web Design and Development studio. We are a team of designer and developer passtionate to
						deliver result for our client. We truly believe every business should have a stunning website and we are
						here to deliver it lcome to Creatix, we are a Hong Kong based Web Design and Development studio. We are
						a team of designer and developer passtionate to deliver result for our client. We truly believe every
						business should have a stunning website and we are here to deliver it
					</p>
				</div>
				<div class="col-md-7 col-md-pull-7">
					<img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/images/content-img-2.jpg" alt="content-image">
				</div>
			</div>
		</div>
	</section>
<div class="clearfix"></div>

	<section class="content">
		<div class="conainer-fluid">
			<div class="col-md-9 col-md-offset-3 margin-top-130">
				<h2 class="section-title text-center">Get in touch!</h2>
				<p class="text-center">come to Creatix, we are a Hong Kong based Web Design and Development studio. We are a
					team of designer and developer passtionate to deliver result for our client. We truly believe every
					business should have a stunning website and we are here to deliver it lcome to Creatix, we are a Hong
					Kong based Web Design and Development studio. We are a team of designer and developer passtionate to
					deliver result for our client. We truly believe every business should have a stunning website and we are
					here to deliver it lcome to Creatix, we are a Hong Kong based Web Design and Development studio. We are
					a team of designer and developer passtionate to deliver result for our client. We truly believe every
					business should have a stunning website and we are here to deliver it
				</p>
			</div>
		</div>
		<div class="clearfix"></div>
		<div 	class="map"
				data-lat="22.278251"
				data-lng="114.174128"
				data-marker="<?php echo get_template_directory_uri(); ?>/assets/images/icons/marker.png"
				data-zoom="10">
		</div>
	</section>
<div class="clearfix"></div>

	<section class="contacts">
		<div class="col-md-6 col-md-offset-1">
			<h2 class="section-title text-left">Our Locations</h2>
			<div class="locations">
				<h4>HONG KONG</h4>
				<p>
					11/F, Kai Tak Commercial Building,<br>
					317-319 Des Voeux Road,<br>
					Sheung Wan, Hong Kong<br>
					Email: <a href="mailto:hello@creatix.com.hk">hello@creatix.com.hk</a>
				</p>
					<h4>HONG KONG</h4>
				<p>
					11/F, Kai Tak Commercial Building,<br>
					317-319 Des Voeux Road,<br>
					Sheung Wan, Hong Kong<br>
					Email: <a href="mailto:hello@creatix.com.hk">hello@creatix.co.uk</a>
				</p>
			</div>
		</div>
		<div class="col-md-7">
			<h2 class="section-title text-left">Shoot us a message!</h2>
			<p>Get a quotation today and our team wil get back to you shortly Get a quotation today and our team wil get
				back to you shortly Get a quotation today and our team wil get back to you shortly.</p>
			<?php echo do_shortcode('[contact-form-7 id="114" title="Contact"]'); ?>
		</div>
	</section>
<div class="clearfix"></div>

		<?php // Show the selected frontpage content.
		if ( have_posts() ) :
			while ( have_posts() ) : the_post();
				//get_template_part( 'template-parts/page/content', 'front-page' );
			endwhile;
		else : // I'm not sure it's possible to have no posts when this page is shown, but WTH.
			//get_template_part( 'template-parts/post/content', 'none' );
		endif; ?>

		<?php
		// Get each of our panels and show the post data.
		if ( 0 !== creatix_panel_count() || is_customize_preview() ) : // If we have pages to show.

			/**
			 * Filter number of front page sections in Creatix.
			 *
			 * @since Creatix 1.0
			 *
			 * @param int $num_sections Number of front page sections.
			 */
			$num_sections = apply_filters( 'creatix_front_page_sections', 4 );
			global $creatixcounter;

			// Create a setting and control for each of the sections available in the theme.
			for ( $i = 1; $i < ( 1 + $num_sections ); $i++ ) {
				$creatixcounter = $i;
				creatix_front_page_section( null, $i );
			}

	endif; // The if ( 0 !== creatix_panel_count() ) ends here. ?>


<?php get_footer();
