<?php get_header(); ?>
<?php
    $args = array(
        'posts_per_page'   => 5,
        'offset'           => 0,
        'category'         => '',
        'category_name'    => '',
        'orderby'          => 'date',
        'order'            => 'DESC',
        'include'          => '',
        'exclude'          => '',
        'meta_key'         => '',
        'meta_value'       => '',
        'post_type'        => 'post',
        'post_mime_type'   => '',
        'post_parent'      => '',
        'author'	       => '',
        'author_name'	   => '',
        'post_status'      => 'publish',
        'suppress_filters' => true
    );
?>
<?php $posts = get_posts( $args ); ?>

<div class="container-fluid">
    <div class="row post-list">
        <div class="col-sm-13 col-sm-offset-1">

                <?php if ($posts) : ?>
                <?php $i = 1; ?>
                <?php foreach ( $posts as $post ) : ?>
                <?php the_post(); ?>
                <?php $featured_img_url = get_the_post_thumbnail_url('full'); ?>
                <?php if ($i % 2 != 0) : ?>
                    <div class="row">
                <?php endif; ?>

                <div class="col-sm-7n5 post">
                    <a href="<?php echo get_the_permalink(); ?>" class="post-thumbnail" style="background-image: url(<?php _e(get_the_post_thumbnail_url($post->ID,'full')); ?>);"></a>
                    <div class="post-content">
                        <h3><?php _e(get_the_title()); ?></h3>
                        <p><?php _e(get_the_excerpt()); ?></p>
                        <a href="<?php the_permalink(); ?>">read more ></a>
                    </div>
                </div>

                <?php if ($i % 2 == 0) : ?>
                    </div>
                <?php endif; ?>
                <?php $i++; ?>
                <?php endforeach; ?>

                <?php else : ?>
                    <p><?php _e( 'Empty, nothing posted so far.' ); ?></p>
                <?php endif; ?>

            </div>
        </div>
    </div>
</div>

<?php get_footer();