<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Creatix
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">

    <?php wp_head(); ?>
</head>

<?php
function getContent($id){
	$content = [];
	$content[0] = get_the_title($id);
	$content[1] = '';
	if( have_rows('post_content', $id) ):
		while ( have_rows('post_content', $id) ) : the_row();
			if( get_row_layout() == 'content_text' ):
				$content[1] .= '<div class="row"><div class="col-sm-11 col-sm-offset-2">' . get_sub_field('content_text_field', $id) . '</div></div>';
            elseif( get_row_layout() == 'content_image' ):
                $post_img = '' . get_sub_field('content_image_field', $id);
                $content[1] .= '<img class="img-responsive" src="' . $post_img . '" alt="post-img">';
            elseif( get_row_layout() == 'content_divider' ):
                $content[1] .= '<section class="section-divider">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-13 col-sm-offset-1 col-xs-15 col-xs-offset-0">
                                <div class="divider"></div>
                            </div>
                        </div>
                    </div>
                </section>
                <div class="clearfix"></div>';
            endif;
        endwhile;
    endif;
    return $content;
}
?>

<body <?php body_class(); ?> id="<?php echo $pagename; ?>">

    <header id="masthead" class="site-header" role="banner">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-13 col-sm-offset-1">
                    <?php if ( has_nav_menu( 'top' ) ) : ?>
                        <?php get_template_part( 'template-parts/navigation/navigation', 'top' ); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </header><!-- #masthead -->
    <?php $img = get_field('header_image'); ?>
    <?php require_once('template-parts/header/header-section.php'); ?>

    <main>
