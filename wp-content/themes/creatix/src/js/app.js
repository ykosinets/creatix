"use strict";

/* Plugins */
//= ../vendor/jquery/dist/jquery.min.js

// /* Bootstrap */
//= ../vendor/bootstrap-sass/assets/javascripts/bootstrap/dropdown.js
//= ../vendor/bootstrap-sass/assets/javascripts/bootstrap/collapse.js
//= ../vendor/bootstrap-sass/assets/javascripts/bootstrap/transition.js
//= ../vendor/bootstrap-sass/assets/javascripts/bootstrap/modal.js
//= ../vendor/bootstrap-sass/assets/javascripts/bootstrap/carousel.js

// /* Owl Carousel 2 */
//= ../vendor/owl.carousel/dist/owl.carousel.min.js

/* Main */
window.app = {
    $body: $("body"),

    init: function () {},

    preventDefault: function (e) {
        e = e || window.event;
        if (e.preventDefault)
            e.preventDefault();
        e.returnValue = false;
    },

    preventDefaultForScrollKeys: function (e) {
        if (this.keys[e.keyCode]) {
            this.preventDefault(e);
            return false;
        }
    },

    disableScroll: function () {
        window.onwheel = this.preventDefault; // modern standard
        window.ontouchmove = this.preventDefault; // mobile
        document.onkeydown = this.preventDefaultForScrollKeys;
    },

    enableScroll: function () {
        window.onwheel = null;
        window.ontouchmove = null;
        document.onkeydown = null;
    },

    isInViewport : function elementInViewport(el) {
        //special bonus for those using jQuery
        if (typeof jQuery === "function" && el instanceof jQuery) {
            el = el[0];
        }

        var rect = el.getBoundingClientRect();

        return (
            rect.top >= 0 &&
            rect.left >= 0 &&
            rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && /*or $(window).height() */
            rect.right <= (window.innerWidth || document.documentElement.clientWidth) /*or $(window).width() */
        );
    },

    switchBodyScrolledClass: function () {
        app.$body[$(window).scrollTop() > 0 ? "addClass" : "removeClass"]("scrolled");
    },

    isMobile: function(){
        return function () {
            var check = false;
            (function (a) {
                if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true;
            })(navigator.userAgent || navigator.vendor || window.opera);
            console.log('mobile=', check);
            return check;
        };
    },

    hoverSlider:{
        el:{},
        scroll: function (direction, el) {
                var speed = 1000;
                var w = el.parent().width() + 120;
                var items = el.find('.item');
                var count = items.length;
                var currentCount = 0;
                var sum = items.eq(0).outerWidth() * count;
                items.each(function(index){
                    if( app.isInViewport($(this)) ){
                        if (direction == "Left"){
                            currentCount = index < 1 ? count - 1 : count - index;
                            return false;
                        }else{
                            if ( $(this).next().length ) {
                                if (!app.isInViewport($(this).next())){
                                    currentCount = index + 1;
                                    return false;
                                }
                            }else{
                                currentCount = index;
                                return false;
                            }
                        }
                    }
                });

                el.parent().animate({
                    scrollLeft: direction == 'Left' ? 0 : sum
                }, speed * (count - currentCount), 'swing');
            },
        init: function() {
            var self = this;
            self.el.parents('.slider').find('.prev').mouseenter(function(e) {
                self.scroll("Left", $(this).parents('.slider').find('.slider-wrapper'));
            });
            self.el.parents('.slider').find('.next').mouseenter(function() {
                self.scroll("Right", $(this).parents('.slider').find('.slider-wrapper'));
            });
            self.el.parents('.slider').find('.prev, .next').mouseleave(function() {
                self.el.parent().stop();
            });
        }
    },

    map:{
        el: $('.map'),
        init: function(){
            var self = this;

            if(self.el.length) {
                var geocoder = new google.maps.Geocoder();
                var bounds = new google.maps.LatLngBounds();
                var address = self.el.data('address').replace("['", '').replace("']", '').replace(/<\/?[^>]+(>|$)/g, "").split("', '") || "New York";
                var latLng;
                var map;

                var myOptions = {
                    scrollwheel: false,
                    navigationControl: false,
                    mapTypeControl: false,
                    scaleControl: false,
                    // draggable: false,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    center: new google.maps.LatLng(41.923, 12.513),
                    zoom: parseInt(self.el.data('zoom')),
                    disableDefaultUI: true
                };
                map = new google.maps.Map(self.el[0], myOptions);

                for (var i = 0; i < address.length; i++) {
                    if (address[i].length) {
                        geocoder.geocode({'address': address[i]}, function (results, status) {
                            if (status == google.maps.GeocoderStatus.OK) {
                                latLng = {
                                    lat: results[0].geometry.location.lat(),
                                    lng: results[0].geometry.location.lng()
                                };
                                setMarker(latLng);
                            } else {
                                console.log('map: ', google.maps.GeocoderStatus)
                            }
                        });
                    }
                }

                var setMarker = function (latLng) {
                    var image = self.el.data('marker');
                    var marker = new google.maps.Marker({
                        position: latLng,
                        map: map,
                        icon: image
                    });
                    bounds.extend(marker.position);
                    map.fitBounds(bounds);
                };
            }else{
                //agency content switcher
                app.agency.el = $('.agency-link');
                app.agency.init();
            }
        }
    },

    formNotifications:{
        el: {},
        init: function(){
            var self = this;
            self.el = self.el || $(window);
            self.el.on({
                'wpcf7invalid': function(e){
                    console.info('mail invalid');
                },
                'wpcf7spam': function(e){
                    console.info('mail spam');
                },
                'wpcf7mailsent': function(e){
                    console.info('mail sent');
                    app.notificationModal.init($('#formNotification'));
                },
                'wpcf7mailfailed': function(e){
                    console.info('mail failed');
                },
                'wpcf7submit': function(e){
                    console.info('form submited');
                }
            });
        }
    },

    notificationModal:{
        el: {},
        init: function(obj, options){
            var self = this;
            self.el = self.el.length ? self.el : obj;
            if (self.el.length){
                self.el.modal(options);
            }
        }
    },

    contentModal:{
        el: {},
        init: function(obj, options){
            var self = this;
            self.el = self.el.length ? self.el : obj;
            if (self.el.length){
                $('.content-modal').on('show.bs.modal', function (e) {
                    var title = obj.find('.post-title-hidden').text();
                    var content = obj.find('.post-content-hidden').html();

                    $(this).find('.post-title-inner').text(title);
                    $(this).find('.post-content-inner').html(content);
                });
                $('.content-modal').modal(options);
            }
        }
    },

    separatedSlider:{
        el: {},
        init: function(){
            var self = this;
            sliderInit(self.el, {
                loop: true,
                margin: 40,
                nav: false,
                dots: true,
                dotsEach: true,
                autoplay: true,
                autoplayTimeout: 3000,
                autoplayHoverPause: true,
                responsive:{
                    0:{
                        items: 2,
                        margin: 10
                    },
                    480:{
                        items: 4,
                        margin: 40
                    },
                    992:{
                        items: 5
                    }
                }
            })
        }
    },

    hashNavigation: {
        el: {},
        scrollTo: function (element, elementType) {
            elementType = elementType || false;


            var pathname = !elementType ? location.pathname.replace(/^\//, "") == element.pathname.replace(/^\//, "") : true;
            var hostname = !elementType ? location.hostname == element.hostname : true;

            if (pathname && hostname) {
                var target = !elementType ? $(element.hash) : $($(element).val());
                target = target.length ? target : $("[name=" + !elementType ? element.hash.slice(1) : $(element).val().slice(1) + "]");
                if (target.length) {
                    $("html, body").animate({
                        scrollTop: target.offset().top
                    }, 500, function () {
                        var $target = $(target);
                        $target.focus();

                        if ($target.is(":focus")) {
                            return false;
                        } else {
                            $target.attr("tabindex", "-1");
                            $target.focus();
                        }
                    });
                }
            }
        },
        init: function () {
            app.hashNavigation.el.on("click", function (e) {
                e.preventDefault();
                app.hashNavigation.scrollTo(this);
            });
        }
    },

    YouTubePlayer:{
        el: 'player', //id of youtube player tag wrapper
        init: function(){
            //default API call's
            window.onPlayerReady = function (e) {
                e.target.playVideo();
            };
            window.onYouTubeIframeAPIReady = function () {
                initPlayer();
            };

            function initPlayer() {
                var player;
                var options = {
                    videoId: $('.youtube-video #player').data("video-id"),
                    playerVars: {rel: 0},
                    events: {
                        "onReady": app.isMobile ? function () {
                            } : onPlayerReady
                    },
                    rel: 0
                };
                if (app.isMobile) {
                    player = new YT.Player('player', options); // "player" is id of DOM element here
                } else {
                    $('.video-overlay').on("click", function () {
                        player = new YT.Player('player', options); // "player" is id of DOM element here
                    });
                }
            }

            var loadAPI = function () {
                var tag = document.createElement("script");
                tag.src = "https://www.youtube.com/iframe_api";
                var firstScriptTag = document.getElementsByTagName('script')[0];
                firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
            }();
        }
    },

    menu: {
        el: {},
        init: function(){
            var self = this;
            self.el.find('.navbar-toggle')
                .on('click', function(){
                    app.$body.toggleClass('menu-open');
                });

            self.el.find('li.dropdown')
                .on({
                    'mouseenter': function() {
                        $(this).addClass('open');
                    },
                    'mouseleave': function() {
                        $(this).removeClass('open');
                    }
                });
        }
    },

    accordion: {
        el:{},
        init: function(){
            var self = this;
            self.el.on("shown.bs.collapse", function (e) {
                var $panel = $(e.target);
                var offset = $panel.offset().top;
                var $titleH = $panel.prev().innerHeight();
                if (offset) {
                    $("html,body").animate({
                        scrollTop: offset - $titleH
                    }, 300);
                }
            });
        }
    },

    agency:{
        el: {},
        init: function(){
            var self = this;
            var address = self.el.data('location').replace(/<\/?[^>]+(>|$)/g, "");
            console.log(address);
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({'address': address}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    console.log(results);
                } else {
                    //console.log('map: ', google.maps.GeocoderStatus)
                }
            });
        }
    }
};

window.isMobile = $(window).width() < 768;
$(window).on("resize", function () {
    window.isMobile = $(window).width() < 768;
});


// slider init whit check for exist + defaults
function sliderInit(obj, options) {
    obj = obj || false;
    options = options || {
            navigation : false,
            slideSpeed : 300,
            paginationSpeed : 400,
            items : 1,
            itemsDesktop : false,
            itemsDesktopSmall : false,
            itemsTablet: false,
            itemsMobile : false,
            autoHeight: false
        };
    if (obj && obj.length) {
        obj.owlCarousel(options);
    }
}

//calls
$(document).ready(function () {
    $('.work-item a').on('click', function(e){
        e.preventDefault();
        app.contentModal.init($(this).parent());
    });

    // Content slider
    sliderInit($('.content-slider'));

    // Form notification reaction
    app.formNotifications.el = $(".wpcf7");
    app.formNotifications.init();

    // separated slider init
    app.separatedSlider.el = $('.slider-separated .owl-carousel');
    app.separatedSlider.init();

    // menu init
    app.menu.el = $('.site-header #site-navigation');
    app.menu.init();

    //bootstrap Carousel init
    $('.slider.right .slider-wrapper').parent().scrollLeft(200000); //set position for second row slider
    if ( !window.isMobile ) {
        app.hoverSlider.el = $('.slider-fluid .slider-wrapper');
        app.hoverSlider.init();
    }

    //hash navidation
    app.hashNavigation.el = $('a[href*="#"]:not([href="#"])');
    app.hashNavigation.init();

    //add "scrolled" to body if page scrolled on more than 1px
    app.switchBodyScrolledClass();
    $(window).on("scroll", function () {
        app.switchBodyScrolledClass();
    });

    //sliders init
    var sliderHighlight = $(".highlights-slider");

    var btns = ["<button type='button' class='btn btn-red btn-next' role='button'></button>", "<button type='button' class='btn btn-red btn-prev' role='button'></button>"];
    sliderInit(sliderHighlight, {
        loop: false,
        margin: 30,
        nav: true,
        navText: btns,
        responsive: {
            0: {
                items: 1
            },
            768: {
                items: 3
            },
            992: {
                items: 4
            }
        },

        onInitialized: function (e) {
            var slider = $(e.target);
            slider.addClass("initialized");
        }
    });

    //accordion
    app.accordion.el = $(".accordion");
    app.accordion.init();
});
