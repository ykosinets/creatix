<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Creatix
 * @since 1.0
 * @version 1.2
 */

?>

	</main><!-- #content -->

		<?php if( !is_page( 'contact-us' ) ) : ?>
			<div class="contact-us-section">
				<div class="container">
					<div class="content-wrapper">
						<div class="content">
							<h3 class="text-red"><?php echo get_field('contact_message','options'); ?></h3>
							<a class="btn btn-primary btn-md" href="<?php echo get_home_url() . '/contact-us'; ?>"><?php echo get_field('contact_button_text','options'); ?></a>
						</div>
						<div class="horn">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/horn.png" alt="horn">
						</div>
					</div>
				</div>
			</div>
		<?php endif; ?>

		<footer id="colophon" class="site-footer" role="contentinfo">
			<!-- Collect the nav links, forms, and other content for toggling -->
			<?php
			wp_nav_menu( array(
				'menu' => 'Footer Menu'
			) );
			?>
			<div class="container-fluid">
				<div class="col-xs-11 col-xs-offset-2 col-sm-15 col-sm-offset-0 copyright">
					All rights reserved 2017 Creatix - A Dragon Lab Company
				</div>
			</div>
		</footer><!-- #colophon -->

		<div class="modal fade wpcf7-modal" id="formNotification">
			<div class="modal-dialog">
				<div class="modal-content text-center">
					<div class="modal-header">
						<h4 class="modal-title">Thank You :)</h4>
					</div>
					<div class="modal-body message">
						Thanks for sending in the message, we’ll be in touch very soon, have a nice day!
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-filled btn-md" data-dismiss="modal">OK</button>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->

		<div class="modal fade content-modal" id="contentModal">
			<div class="modal-dialog modal-lg">
				<div class="modal-content text-center">
					<div class="modal-header">
						<h4 class="modal-title post-title-inner section-title text-center"></h4><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<div class="modal-body message">
						<div class="post-content-inner"></div>
					</div>
					<div class="modal-footer text-center">
						<button type="button" class="btn btn-filled btn-md" data-dismiss="modal">Back</button>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->

<?php wp_footer(); ?>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=<?php echo get_field('gmaps_key', options); ?>&callback=window.app.map.init" type="text/javascript"></script>
</body>
</html>
