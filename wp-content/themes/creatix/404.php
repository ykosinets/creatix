<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package WordPress
 * @subpackage Creatix
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<section class="content simple">
	<div class="container">
		<div class="row">
			<div class="col-md-9 col-md-offset-3">
				<h2 class="section-title text-center">404 Page not fount</h2>
				<div class="content-wrapper text-center margin-bottom-65">We cant find page you are looking for.</div>
			</div>
		</div>
	</div>
</section>

<?php get_footer();

