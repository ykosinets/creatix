<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'creatix');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'X!/4;<a^zphZ:.k/_yYVn(?AR/ _y? g@bxZV4nVgYl.xuu+e29/q qYeOA*%ptc');
define('SECURE_AUTH_KEY',  'DS_N`ph/>De-SWXIP+G-9N7Q`,n*xlshSqbQ8=Rmdst1Lp:a~Dp#m]Bwpxchu|gy');
define('LOGGED_IN_KEY',    '9D,BPo[*e]Y-L5Y-W|>m||KB.`zjWCQs(Acee:yy9+AdG>B9&>t}_ Uxi[AB^M6a');
define('NONCE_KEY',        '.)~xJnj$Fe8LXL59$`e~B$5]>CWAZ./S@#b~7Wj-ec=9UdyA3o+^Aj C3^};?#-|');
define('AUTH_SALT',        's!]8.zH8xC.kw9j4#3l^BA=f]bf#CfIz?]BV_oLb:(q*zPo?0AQ)]x9Fo`[^]@x5');
define('SECURE_AUTH_SALT', 'v.ZfRz0/b)%.6b}G0}CzOp-eg=C5z_tZh/_*sRRu6`9X7 gzftXkA@R8Bux51>}T');
define('LOGGED_IN_SALT',   'ETHL]7FoMS`;Z/H{EN2=:] 3rdvGvvrcFuTgSu&B FYwO{Z2=[Z QA3*)Lpcu,)7');
define('NONCE_SALT',       '^%?-`2)<0csL<o*>DDf,Zug=I-P9Py]G n&Hk]0,9+]:Yn}nxm$uo?=Qi+TPs[<D');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
